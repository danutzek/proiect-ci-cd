#!/bin/sh

catch=0

for var in $@; do
  if [ $catch -eq 1 ]; then
    logs_path=$var
    break
  fi
  if [ "$var" == "--logs-path" ]; then
    catch=1
  fi
done

mkdir -p logs_path

/webserver.py $@
