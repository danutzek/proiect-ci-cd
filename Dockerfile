FROM alpine:latest

RUN apk add python3

COPY webserver.py /webserver.py 

COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

ENTRYPOINT [ "/entrypoint.sh" ]

CMD [ "start", "--host", "0.0.0.0", "--port", "80", "--logs-path", "/var/log", "--workers", "2" ]
